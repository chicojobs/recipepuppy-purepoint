# README #

This project requires Xcode 9 or higher.

The app is a single UITableViewController that receives update after each keystroke in the search field.
My choice for the architecture is **MVVM** which truly shines if combined with **RxSwift**. By doing so, the View Model becomes *highly testable*, as you can see in the included Unit Tests.
For unit testing I used **OHHTTPStubs** to mock network responses.