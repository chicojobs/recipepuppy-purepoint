//
//  ViewModelTests.swift
//  recipepuppy-purepointTests
//
//  Created by Enrico Querci on 16/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import RxBlocking
import OHHTTPStubs

class ViewModelTests: XCTestCase {
    
    var viewModel: ViewModel!
    let disposeBag = DisposeBag()
    var concurrentScheduler: ConcurrentDispatchQueueScheduler!

    override func setUp() {
        super.setUp()
        concurrentScheduler = ConcurrentDispatchQueueScheduler(qos: .default)
        setupStubs()
    }
    
    func testRecipesResponse() {
        let searchStringObservable = Observable.of("potato")
        self.viewModel = ViewModel(searchBarObservable: searchStringObservable.asObservable())
        
        let result = try! viewModel.recipesObservable.toBlocking().first()!
        XCTAssertEqual(result.count, 20)
    }

    func testRecipesNoResponse() {
        let searchStringObservable = Observable.of("abcdefghilmopqrstuvz")
        self.viewModel = ViewModel(searchBarObservable: searchStringObservable.asObservable())
        
        let result = try! viewModel.recipesObservable.toBlocking().first()!
        XCTAssertEqual(result.count, 0)
    }
}

// MARK: - Private Methods
extension ViewModelTests {
    private func setupStubs() {
        // Page 1
        stub(condition: isHost("www.recipepuppy.com") && containsQueryParams(["page": "1"])) { request in
            return OHHTTPStubsResponse(
                fileAtPath: OHPathForFile("potato_page_1.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"])
        }
        
        // Page 2
        stub(condition: isHost("www.recipepuppy.com") && containsQueryParams(["page": "2"])) { request in
            return OHHTTPStubsResponse(
                fileAtPath: OHPathForFile("potato_page_2.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"])
        }
        
        // Empty Response
        stub(condition: isHost("www.recipepuppy.com") && containsQueryParams(["q": "abcdefghilmopqrstuvz"])) { request in
            return OHHTTPStubsResponse(
                fileAtPath: OHPathForFile("empty_response.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"])
        }
    }
}
