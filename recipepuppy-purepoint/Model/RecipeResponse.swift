//
//  RecipeResponse.swift
//  recipepuppy-purepoint
//
//  Created by Enrico Querci on 14/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

class RecipeResponse: Codable {
    let version: Double
    let title: String
    let href: String
    let results: [Recipe]
}
