//
//  Recipe.swift
//  recipepuppy-purepoint
//
//  Created by Enrico Querci on 14/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

class Recipe: Codable {
    let title: String
    let href: URL
    let ingredients: String
    let thumbnail: String
}
