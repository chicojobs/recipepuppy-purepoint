//
//  ViewController.swift
//  recipepuppy-purepoint
//
//  Created by Enrico Querci on 14/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let disposeBag = DisposeBag()
    let cellIdentifier = "recipeCellIdentifier"
    var viewModel: ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        
        viewModel = ViewModel(searchBarObservable: searchBar.rx.text.orEmpty.asObservable())
        viewModel.recipesObservable.bind(to: tableView.rx.items(cellIdentifier: cellIdentifier, cellType: UITableViewCell.self)) { (row, recipe, cell) in
            cell.textLabel?.text = recipe.title
            cell.selectionStyle = .none
        }.addDisposableTo(disposeBag)
    }
}

extension ViewController: UITableViewDelegate, UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

